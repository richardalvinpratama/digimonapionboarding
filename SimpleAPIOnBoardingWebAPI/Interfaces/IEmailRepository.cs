﻿namespace SimpleAPIOnBoardingWebAPI.Interfaces
{
    public interface IEmailRepository
    {
        void SendEmail(string emailReceiver);
    }
}
