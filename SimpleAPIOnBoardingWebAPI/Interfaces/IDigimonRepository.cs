﻿using SimpleAPIOnBoardingWebAPI.Dto;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Interfaces
{
    public interface IDigimonRepository
    {
        ICollection<Digimon> GetDigimons();

        Digimon GetDigimon(int id);
        Digimon GetDigimon(string name);
        decimal GetDigimonRating(int digiId);
        Digimon GetDigimonTrimToUpper(DigimonDto digimonCreate);
        bool DigimonExists(int digiId);
        bool CreateDigimon(int ownerId, int categoryId, Digimon digimon);
        bool UpdateDigimon(int ownerId, int categoryId, Digimon digimon);
        bool DeleteDigimon(Digimon digimon);
        bool Save();
    }
}
