﻿using Microsoft.EntityFrameworkCore;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Data
{
    public class DataContext :DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Digimon> Digimon { get; set; }
        public DbSet<DigimonOwner> DigimonOwners { get; set; }
        public DbSet<DigimonCategory> DigimonCategories { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Reviewer> Reviewers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DigimonCategory>()
                     .HasKey(dc => new { dc.DigimonId, dc.CategoryId });
            modelBuilder.Entity<DigimonCategory>()
                    .HasOne(d => d.Digimon)
                    .WithMany(dc => dc.DigimonCategories)
                    .HasForeignKey(d => d.DigimonId);
            modelBuilder.Entity<DigimonCategory>()
                    .HasOne(d => d.Category)
                    .WithMany(dc => dc.DigimonCategories)
                    .HasForeignKey(c => c.CategoryId);

            modelBuilder.Entity<DigimonOwner>()
                    .HasKey(po => new { po.DigimonId, po.OwnerId });
            modelBuilder.Entity<DigimonOwner>()
                    .HasOne(p => p.Digimon)
                    .WithMany(pc => pc.DigimonOwners)
                    .HasForeignKey(p => p.DigimonId);
            modelBuilder.Entity<DigimonOwner>()
                    .HasOne(p => p.Owner)
                    .WithMany(pc => pc.DigimonOwners)
                    .HasForeignKey(c => c.OwnerId);
        }
    }
}
