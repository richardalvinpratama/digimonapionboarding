﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleAPIOnBoardingWebAPI.Dto;
using SimpleAPIOnBoardingWebAPI.Interfaces;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DigimonController : Controller
    {
        private readonly IDigimonRepository _digimonRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IMapper _mapper;

        public DigimonController(IDigimonRepository digimonRepository,
            IReviewRepository reviewRepository,
            IMapper mapper)
        {
            _digimonRepository = digimonRepository;
            _reviewRepository = reviewRepository;
            _mapper = mapper;
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Digimon>))]
        public IActionResult getDigimons()
        {
            var digimons = _mapper.Map<List<DigimonDto>>(_digimonRepository.GetDigimons());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(digimons);
        }

        [HttpGet("{digiId}")]
        [ProducesResponseType(200, Type = typeof(Digimon))]
        [ProducesResponseType(400)]
        public IActionResult GetDigimon(int digiId)
        {
            if (!_digimonRepository.DigimonExists(digiId))
                return NotFound();

            var digimon = _mapper.Map<DigimonDto>(_digimonRepository.GetDigimon(digiId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(digimon);
        }

        [HttpGet("{digiId}/rating")]
        [ProducesResponseType(200, Type = typeof(decimal))]
        [ProducesResponseType(400)]
        public IActionResult GetDigimonRating(int digiId)
        {
            if (!_digimonRepository.DigimonExists(digiId))
                return NotFound();

            var rating = _digimonRepository.GetDigimonRating(digiId);

            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(rating);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public IActionResult CreatePokemon([FromQuery] int ownerId, [FromQuery] int catId, [FromBody] DigimonDto digimonCreate)
        {
            if (digimonCreate == null)
                return BadRequest(ModelState);

            var digimons = _digimonRepository.GetDigimonTrimToUpper(digimonCreate);

            if (digimons != null)
            {
                ModelState.AddModelError("", "Owner already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var digimonMap = _mapper.Map<Digimon>(digimonCreate);


            if (!_digimonRepository.CreateDigimon(ownerId, catId, digimonMap))
            {
                ModelState.AddModelError("", "Something went wrong while savin");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{digiId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult UpdateDigimon(int digiId,
            [FromQuery] int ownerId, [FromQuery] int catId,
            [FromBody] DigimonDto updatedDigimon)
        {
            if (updatedDigimon == null)
                return BadRequest(ModelState);

            if (digiId != updatedDigimon.Id)
                return BadRequest(ModelState);

            if (!_digimonRepository.DigimonExists(digiId))
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest();

            var digimonMap = _mapper.Map<Digimon>(updatedDigimon);

            if (!_digimonRepository.UpdateDigimon(ownerId, catId, digimonMap))
            {
                ModelState.AddModelError("", "Something went wrong updating owner");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{pokeId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult DeleteDigimon(int digiId)
        {
            if (!_digimonRepository.DigimonExists(digiId))
            {
                return NotFound();
            }

            var reviewsToDelete = _reviewRepository.GetReviewsOfADigimon(digiId);
            var digimonToDelete = _digimonRepository.GetDigimon(digiId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_reviewRepository.DeleteReviews(reviewsToDelete.ToList()))
            {
                ModelState.AddModelError("", "Something went wrong when deleting reviews");
            }

            if (!_digimonRepository.DeleteDigimon(digimonToDelete))
            {
                ModelState.AddModelError("", "Something went wrong deleting owner");
            }

            return NoContent();
        }

    }
}
