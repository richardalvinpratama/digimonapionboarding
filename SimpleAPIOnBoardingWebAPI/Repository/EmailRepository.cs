﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using SimpleAPIOnBoardingWebAPI.Interfaces;

namespace SimpleAPIOnBoardingWebAPI.Repository
{
    public class EmailRepository : IEmailRepository
    {
        private readonly IConfiguration _configuration;

        public EmailRepository(IConfiguration config)
        {
            _configuration = config;
        }

        public void SendEmail(string emailReceiver)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailEtheral:EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(emailReceiver));
            email.Subject = "Verifikasi Akun";
            email.Body = new TextPart(TextFormat.Html)
            {
                Text = "Ini adalah email verifikasi akun kamu di aplikasi ABC. Klik link dibawah untuk verify account"
            };

            using var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailEtheral:EmailHost").Value, 587, SecureSocketOptions.StartTls);
            smtp.Authenticate(_configuration.GetSection("EmailEtheral:EmailUserName").Value, _configuration.GetSection("EmailEtheral:EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
