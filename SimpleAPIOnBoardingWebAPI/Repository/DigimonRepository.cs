﻿using SimpleAPIOnBoardingWebAPI.Data;
using SimpleAPIOnBoardingWebAPI.Dto;
using SimpleAPIOnBoardingWebAPI.Interfaces;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Repository
{
    public class DigimonRepository : IDigimonRepository
    {
        private readonly DataContext _context;
        public DigimonRepository(DataContext context)
        {
            _context = context;
        }

        public bool DigimonExists(int digiId)
        {
            return _context.Digimon.Any(p => p.Id == digiId);
        }

        public Digimon GetDigimon(int id)
        {
            return _context.Digimon.Where(p => p.Id == id).FirstOrDefault();
        }

        public Digimon GetDigimon(string name)
        {
            return _context.Digimon.Where(p => p.Name == name).FirstOrDefault();
        }

        public decimal GetDigimonRating(int digiId)
        {
            var review = _context.Reviews.Where(p => p.Digimon.Id == digiId);
            if (review.Count() <= 0)
                return 0;

            return ((decimal)review.Sum(r => r.Rating) / review.Count());
        }

        public ICollection<Digimon> GetDigimons()
        {
            return _context.Digimon.OrderBy(p => p.Id).ToList();
        }

        public bool CreateDigimon(int ownerId, int categoryId, Digimon digimon)
        {
            var digimonOwnerEntity = _context.Owners.Where(a => a.Id == ownerId).FirstOrDefault();
            var category = _context.Categories.Where(a => a.Id == categoryId).FirstOrDefault();

            var digimonOwner = new DigimonOwner()
            {
                Owner = digimonOwnerEntity,
                Digimon = digimon,
            };

            _context.Add(digimonOwner);

            var digimonCategory = new DigimonCategory()
            {
                Category = category,
                Digimon = digimon,
            };

            _context.Add(digimonCategory);

            _context.Add(digimon);

            return Save();
        }

        public Digimon GetDigimonTrimToUpper(DigimonDto digimonCreate)
        {
            return GetDigimons().Where(c => c.Name.Trim().ToUpper() == digimonCreate.Name.TrimEnd().ToUpper())
                .FirstOrDefault();
        }

        public bool DeleteDigimon(Digimon digimon)
        {
            _context.Remove(digimon);
            return Save();
        }

        public bool UpdateDigimon(int ownerId, int categoryId, Digimon digimon)
        {
            _context.Update(digimon);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
