﻿using SimpleAPIOnBoardingWebAPI.Data;
using SimpleAPIOnBoardingWebAPI.Interfaces;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private DataContext _context;
        public CategoryRepository(DataContext context)
        {
            _context = context;
        }  
        public bool CategoryExists(int id)
        {
            return _context.Categories.Any(c => c.Id == id);
        }

        public ICollection<Category> GetCategories()
        {
            return _context.Categories.ToList();
        }

        public Category GetCategory(int id)
        {
            return _context.Categories.Where(e => e.Id == id).FirstOrDefault();
        }

        public ICollection<Digimon> GetDigimonByCategory(int categoryId)
        {
            return _context.DigimonCategories.Where(e => e.CategoryId == categoryId).Select(c => c.Digimon).ToList();
        }

        public bool CreateCategory(Category category)
        {
            //Change Tracker
            //add, updating, modifying
            _context.Add(category);
            return Save();
        }
        public bool UpdateCategory(Category category)
        {
            _context.Update(category);
            return Save();
        }
        public bool DeleteCategory(Category category)
        {
            _context.Remove(category);
            return Save();
        }
        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true: false;
        }
    }
}
