﻿using SimpleAPIOnBoardingWebAPI.Data;
using SimpleAPIOnBoardingWebAPI.Interfaces;
using SimpleAPIOnBoardingWebAPI.Models;

namespace SimpleAPIOnBoardingWebAPI.Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly DataContext _context;

        public OwnerRepository(DataContext context)
        {
            _context = context;
        }

        public bool CreateOwner(Owner owner)
        {
            _context.Add(owner);
            return Save();
        }

        public bool DeleteOwner(Owner owner)
        {
            _context.Remove(owner);
            return Save();
        }

        public Owner GetOwner(int ownerId)
        {
            return _context.Owners.Where(o => o.Id == ownerId).FirstOrDefault();
        }

        public ICollection<Owner> GetOwnerOfADigimon(int digiId)
        {
            return _context.DigimonOwners.Where(p => p.Digimon.Id == digiId).Select(o => o.Owner).ToList();
        }

        public ICollection<Owner> GetOwners()
        {
            return _context.Owners.ToList();
        }

        public ICollection<Digimon> GetDigimonByOwner(int ownerId)
        {
            return _context.DigimonOwners.Where(p => p.Owner.Id == ownerId).Select(p => p.Digimon).ToList();
        }

        public bool OwnerExists(int ownerId)
        {
            return _context.Owners.Any(o => o.Id == ownerId);
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }

        public bool UpdateOwner(Owner owner)
        {
            _context.Update(owner);
            return Save();
        }
    }
}
