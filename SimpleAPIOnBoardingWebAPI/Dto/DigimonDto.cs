﻿namespace SimpleAPIOnBoardingWebAPI.Dto
{
    public class DigimonDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
