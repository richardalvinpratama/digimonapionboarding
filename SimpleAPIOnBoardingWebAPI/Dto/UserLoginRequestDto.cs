﻿using System.ComponentModel.DataAnnotations;

namespace SimpleAPIOnBoardingWebAPI.Dto
{
    public class UserLoginRequestDto
    {
        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;
        [Required, MinLength(6, ErrorMessage = "Must have 6 Characters")]
        public string Password { get; set; } = string.Empty;
    }
}
