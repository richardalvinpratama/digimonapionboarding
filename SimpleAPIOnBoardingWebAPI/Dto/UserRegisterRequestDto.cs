﻿using System.ComponentModel.DataAnnotations;

namespace SimpleAPIOnBoardingWebAPI.Dto
{
    public class UserRegisterRequestDto
    {
        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;
        [Required, MinLength(6, ErrorMessage = "Must have 6 Characters")]
        public string Password { get; set; } = string.Empty;
        [Required, Compare("Password")]
        public string ConfirmPassword { get; set; } = string.Empty;
    }
}
