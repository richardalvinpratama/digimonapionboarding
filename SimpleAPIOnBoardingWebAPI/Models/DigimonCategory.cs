﻿namespace SimpleAPIOnBoardingWebAPI.Models
{
    public class DigimonCategory
    {
        public int DigimonId { get; set; }
        public int CategoryId { get; set; }

        public Digimon Digimon { get; set; }
        public Category Category { get; set; }

    }
}
