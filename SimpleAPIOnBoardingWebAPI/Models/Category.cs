﻿namespace SimpleAPIOnBoardingWebAPI.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<DigimonCategory> DigimonCategories { get; set; }
    }
}
