﻿namespace SimpleAPIOnBoardingWebAPI.Models
{
    public class Digimon
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public ICollection<Review> Reviews { get; set; }

        public ICollection<DigimonOwner> DigimonOwners { get; set; }
        public ICollection<DigimonCategory> DigimonCategories { get; set; }
    }
}
