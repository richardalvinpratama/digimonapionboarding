﻿namespace SimpleAPIOnBoardingWebAPI.Models
{
    public class DigimonOwner
    {
        public int DigimonId { get; set; }
        public int OwnerId { get; set; }
        public Digimon Digimon { get; set; }
        public Owner Owner { get; set; }
    }
}
